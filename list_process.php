
<?php
	//include connection file
	include_once("connection.php");

	$db = new dbObj();
	$connString =  $db->getConnstring();

	$params = $_REQUEST;

	$action = isset($params['action']) != '' ? $params['action'] : '';
	$lstCls = new ListMaster($connString);

	switch($action) {
	 case 'add':
		$lstCls->insertList($params);
	 break;
	 case 'edit':
		$lstCls->updateList($params);
	 break;
	 case 'delete':
		$lstCls->deleteList($params);
	 break;
	 default:
	 $lstCls->getLists($params);
	 return;
	}

	class ListMaster {
	protected $conn;
	protected $data = array();
	function __construct($connString) {
		$this->conn = $connString;
	}

	public function getLists($params) {

		$this->data = $this->getRecords($params);

		echo json_encode($this->data);
	}
	function insertList($params) {
		$data = array();
		$created_date = date('Y-m-d H:i:s');
		$params["list_id"] = 99;
		$created_by = 1; // You can set up creator ID once login process implemented
		$sql = "INSERT INTO `list_master` (list_name, created_date, created_by) VALUES('" . $params["list_name"] . "','" . $created_date . "','" . $created_by . "');  ";

		echo $result = mysqli_query($this->conn, $sql) or die("error to insert List data");

	}


	function getRecords($params) {
		$rp = isset($params['rowCount']) ? $params['rowCount'] : 10;

		if (isset($params['current'])) { $page  = $params['current']; } else { $page=1; };
        $start_from = ($page-1) * $rp;

		$sql = $sqlRec = $sqlTot = $where = '';

		if( !empty($params['searchPhrase']) ) {
			$where .=" WHERE ";
			$where .=" ( list_name LIKE '%".$params['searchPhrase']."%') ";
		 }
	   if( !empty($params['sort']) ) {
			$where .=" ORDER By ".key($params['sort']) .' '.current($params['sort'])." ";
		}
	   // getting total number records without any search
		$sql = "SELECT * FROM `list_master` ";
		$sqlTot .= $sql;
		$sqlRec .= $sql;

		//concatenate search sql if value exist
		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		if ($rp!=-1)
		$sqlRec .= " LIMIT ". $start_from .",".$rp;

		$qtot = mysqli_query($this->conn, $sqlTot) or die("error to fetch tot Lists data");
		$queryRecords = mysqli_query($this->conn, $sqlRec) or die("error to fetch Lists data");

		while( $row = mysqli_fetch_assoc($queryRecords) ) {
			$data[] = $row;
		}

		$json_data = array(
			"current"            => intval($params['current']),
			"rowCount"            => 10,
			"total"    => intval($qtot->num_rows),
			"rows"            => $data   // total data array
			);

		return $json_data;
	}
	function updateList($params) {
		$data = array();
		$modified_date = date('Y-m-d H:i:s');
		$modified_by = 1; // we can set up session user id who will be updating this data
		//print_R($_POST);die;
		$sql = "Update `list_master` set list_name = '" . $params["edit_list_name"] . "', modified_date='" . $modified_date."', modified_by='" . $modified_by . "' WHERE id='".$_POST["edit_id"]."'";

		echo $result = mysqli_query($this->conn, $sql) or die("error to update List data");
	}

	function deleteList($params) {
		$data = array();
		//print_R($_POST);die;
		$sql = "delete from `list_master` WHERE id='".$params["id"]."'";

		echo $result = mysqli_query($this->conn, $sql) or die("error to delete List data");
	}
}
?>
