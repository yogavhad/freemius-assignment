<?php
GLOBAL $includePath;
GLOBAL $assetsUrl;
$includePath = '/';

require_once './lib/router.php';
$router = new Router();
return $router->go(array(
	'/'=>'list.php',
	'list'=>'list.php',
	'items'=> 'items.php',
));
