
<?php
	//include connection file
	include_once("connection.php");

	$db = new dbObj();
	$connString =  $db->getConnstring();

	$params = $_REQUEST;

	$action = isset($params['action']) != '' ? $params['action'] : '';
	$empCls = new Items($connString);

	switch($action) {
	 case 'add':
		$empCls->insertItems($params);
	 break;
	 case 'edit':
		$empCls->updateItems($params);
	 break;
	 case 'delete':
		$empCls->deleteItems($params);
	 break;
	 default:
	 $empCls->getItemss($params);
	 return;
	}

	class Items {
	protected $conn;
	protected $data = array();
	function __construct($connString) {
		$this->conn = $connString;
	}

	public function getItemss($params) {

		$this->data = $this->getRecords($params);

		echo json_encode($this->data);
	}
	function insertItems($params) {
		$data = array();
		$created_date = date('Y-m-d H:i:s');
		$created_by = 1; // You can set up creator ID once login process implemented
		$sql = "INSERT INTO `items` (list_id,item_name, created_date, created_by) VALUES('" . $params["list_id"] . "', '" . $params["item_name"] . "','" . $created_date . "','" . $created_by . "');  ";

		echo $result = mysqli_query($this->conn, $sql) or die("error to insert Items data");

	}


	function getRecords($params) {
		$rp = isset($params['rowCount']) ? $params['rowCount'] : 10;

		if (isset($params['current'])) { $page  = $params['current']; } else { $page=1; };
        $start_from = ($page-1) * $rp;

		$sql = $sqlRec = $sqlTot = $where = '';
$where .=" WHERE 1 and list_id = ".$_REQUEST['list_id'];
		if( !empty($params['searchPhrase']) ) {

			$where .=" and ( item_name LIKE '%".$params['searchPhrase']."%') ";

		 }
	   if( !empty($params['sort']) ) {
			$where .=" ORDER By ".key($params['sort']) .' '.current($params['sort'])." ";
		}
	   // getting total number records without any search
		$sql = "SELECT * FROM `Items` ";
		$sqlTot .= $sql;
		$sqlRec .= $sql;

		//concatenate search sql if value exist
		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		if ($rp!=-1)
		$sqlRec .= " LIMIT ". $start_from .",".$rp;


		$qtot = mysqli_query($this->conn, $sqlTot) or die("error to fetch tot Itemss data");
		$queryRecords = mysqli_query($this->conn, $sqlRec) or die("error to fetch Itemss data");

		while( $row = mysqli_fetch_assoc($queryRecords) ) {
			$data[] = $row;
		}

		$json_data = array(
			"current"            => intval($params['current']),
			"rowCount"            => 10,
			"total"    => intval($qtot->num_rows),
			"rows"            => $data   // total data array
			);

		return $json_data;
	}
	function updateItems($params) {
		$data = array();
		$modified_date = date('Y-m-d H:i:s');
		$modified_by = 1; // we can set up session user id who will be updating this data
		//print_R($_POST);die;
		$sql = "Update `Items` set item_name = '" . $params["edit_item_name"] . "', modified_date='" . $modified_date."', modified_by='" . $modified_by . "' WHERE id='".$_POST["edit_id"]."'";

		echo $result = mysqli_query($this->conn, $sql) or die("error to update Items data");
	}

	function deleteItems($params) {
		$data = array();
		//print_R($_POST);die;
		$sql = "delete from `Items` WHERE id='".$params["id"]."'";

		echo $result = mysqli_query($this->conn, $sql) or die("error to delete Items data");
	}
}
?>
