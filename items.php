<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Items</title>
<link rel="stylesheet" href="dist/bootstrap.min.css" type="text/css" media="all">
<link href="dist/jquery.bootgrid.css" rel="stylesheet" />
<script src="dist/jquery-1.11.1.min.js"></script>
<script src="dist/bootstrap.min.js"></script>
<script src="dist/jquery.bootgrid.min.js"></script>
</head>
<body>
	<div class="container">
      <div class="">
        <h1>Item List</h1>
        <div class="col-sm-8">
					<a href='./' class='btn btn-warning'>Back</a>
		<div class="well clearfix">
			<div class="pull-right"><button type="button" class="btn btn-xs btn-primary" id="command-add" data-row-id="0">
			<span class="glyphicon glyphicon-plus"></span> Add Items</button></div></div>
		<table id="Items_grid" class="table table-condensed table-hover table-striped" width="60%" cellspacing="0" data-toggle="bootgrid">
			<thead>
				<tr>
					<th data-column-id="id" data-type="numeric" data-identifier="true">ItemID</th>
					<th data-column-id="item_name">Item Name</th>
					<th data-column-id="commands" data-formatter="commands" data-sortable="false">Commands</th>
				</tr>
			</thead>
		</table>
    </div>
      </div>
    </div>

<div id="add_model" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add Items</h4>
            </div>
						  <form method="post" id="frm_add">
            <div class="modal-body">

				<input type="hidden" value="add" name="action" id="action">
                  <div class="form-group">
                    <label for="name" class="control-label">Item Name:</label>
                    <input type="text" class="form-control" id="item_name" name="item_name"/>
                  </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="btn_add" class="btn btn-primary">Save</button>
            </div>
			</form>
        </div>
    </div>
</div>
<div id="edit_model" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit Items</h4>
            </div>
						  <form method="post" id="frm_edit">
            <div class="modal-body">

				<input type="hidden" value="edit" name="action" id="action">
				<input type="hidden" value="0" name="edit_id" id="edit_id">
                  <div class="form-group">
                    <label for="name" class="control-label">Name:</label>
                    <input type="text" class="form-control" id="edit_item_name" name="edit_item_name"/>
                  </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="btn_edit" class="btn btn-primary">Save</button>
            </div>
			</form>
						<input type="text" name="list_id" id="list_id" value="<?php echo $_REQUEST['list']; ?>" />
        </div>
    </div>
</div>

<script type="text/javascript">
$( document ).ready(function() {


	var grid = $("#Items_grid").bootgrid({
		ajax: true,
		rowSelect: true,
		post: function ()
		{
			/* To accumulate custom parameter with the request object */
			return {
				id: "b0df282a-0d67-40e5-8558-c9e93b7befed"
			};
		},

		url: "items_process.php?list_id=<?php echo $_REQUEST['list']; ?>",

		formatters: {
		        "commands": function(column, row)
		        {
		            return "<button type=\"button\" class=\"btn btn-xs btn-default command-edit\" data-row-id=\"" + row.id + "\"><span class=\"glyphicon glyphicon-edit\"></span></button> " +
		                "<button type=\"button\" class=\"btn btn-xs btn-default command-delete\" data-row-id=\"" + row.id + "\"><span class=\"glyphicon glyphicon-trash\"></span></button>";
		        }
		    }
   }).on("loaded.rs.jquery.bootgrid", function()
{
    /* Executes after data is loaded and rendered */
    grid.find(".command-edit").on("click", function(e)
    {
        //alert("You pressed edit on row: " + $(this).data("row-id"));
			var ele =$(this).parent();
			var g_id = $(this).parent().siblings(':first').html();
            var g_name = $(this).parent().siblings(':nth-of-type(2)').html();
console.log(g_id);
                    console.log(g_name);

		//console.log(grid.data());//
		$('#edit_model').modal('show');
					if($(this).data("row-id") >0) {

                                // collect the data
                                $('#edit_id').val(ele.siblings(':first').html()); // in case we're changing the key
                                $('#edit_item_name').val(ele.siblings(':nth-of-type(2)').html());

					} else {
					 alert('Now row selected! First select row, then click edit button');
					}
    }).end().find(".command-delete").on("click", function(e)
    {

		var conf = confirm('Delete ' + $(this).data("row-id") + ' items?');
		                    if(conf){
                                $.post('items_process.php', { id: $(this).data("row-id"), action:'delete'}
                                    , function(){
                                        // when ajax returns (callback),
										$("#Items_grid").bootgrid('reload');
                                });
								//$(this).parent('tr').remove();
								//$("#Items_grid").bootgrid('remove', $(this).data("row-id"))
                    }
    });
		<?php if(isset($_REQUEST['new'])){ ?>
			$('#command-add').trigger("click");
		<?php } ?>
});

function ajaxAction(action) {
				data = $("#frm_"+action).serializeArray();
				$.ajax({
				  type: "POST",
				  url: "items_process.php?list_id=<?php echo $_REQUEST['list']; ?>",
				  data: data,
				  dataType: "json",
				  success: function(response)
				  {

					$('#'+action+'_model').modal('hide');
						alert('Item added / updated successfully');
					window.location.href = "list";

				  }
				});
			}

			$( "#command-add" ).click(function() {
					$('#item_name').val('');
			  $('#add_model').modal('show');
			});
			$( "#btn_add" ).click(function() {
				var item_name = $('#item_name').val();
				if($.trim(item_name) == ""){
					alert('Please add item name');
					return false;
				}
			  ajaxAction('add');
			});
			$( "#btn_edit" ).click(function() {
				var edit_item_name = $('#edit_item_name').val();
				if($.trim(edit_item_name) == ""){
					alert('Please add item name');
					return false;
				}
			  ajaxAction('edit');
			});
});
</script>
