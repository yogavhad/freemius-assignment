/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.5.5-10.1.13-MariaDB : Database - freemius
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`freemius` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `freemius`;

/*Table structure for table `items` */

DROP TABLE IF EXISTS `items`;

CREATE TABLE `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `list_id` int(11) DEFAULT NULL COMMENT 'FK of Master List',
  `item_name` varchar(200) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `items` */

insert  into `items`(`id`,`list_id`,`item_name`,`created_date`,`created_by`,`modified_date`,`modified_by`) values (1,99,'Yogesh 2','2019-10-29 07:55:48',1,'2019-10-29 07:56:08',1),(6,99,'Yogesh 3','2019-10-29 08:33:45',1,NULL,NULL),(7,99,'Yogesh 4','2019-10-29 08:35:07',1,NULL,NULL);

/*Table structure for table `list_master` */

DROP TABLE IF EXISTS `list_master`;

CREATE TABLE `list_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `list_name` varchar(200) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `list_master` */

insert  into `list_master`(`id`,`list_name`,`created_date`,`created_by`,`modified_date`,`modified_by`) values (1,'List 1','2019-10-29 08:31:34',1,NULL,NULL),(2,'List 2','2019-10-29 09:53:35',1,NULL,NULL),(3,'List 3','2019-10-29 09:53:54',1,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
